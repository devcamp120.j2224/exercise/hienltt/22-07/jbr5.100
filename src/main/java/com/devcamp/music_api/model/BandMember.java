package com.devcamp.music_api.model;

public class BandMember extends Composer{
    private String instrument;

    public BandMember(String firstname, String lastname, String stagename, String instrument) {
        super(firstname, lastname, stagename);
        this.instrument = instrument;
    }

    public String getInstrument() {
        return instrument;
    }

    public void setInstrument(String instrument) {
        this.instrument = instrument;
    }

}
