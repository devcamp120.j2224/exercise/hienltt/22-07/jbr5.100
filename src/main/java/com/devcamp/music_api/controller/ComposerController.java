package com.devcamp.music_api.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.music_api.model.*;
import com.devcamp.music_api.service.ComposerService;

@RestController
@CrossOrigin
public class ComposerController {
    @Autowired
    ComposerService composerService;

    @GetMapping("/composers")
    public ArrayList<Band> getListBand(){
        ArrayList<Band> lBands = composerService.getAllBands();

        return lBands;
    }

    @GetMapping("/artists")
    public ArrayList<Artist> getListArtists(){
        ArrayList<Artist> lisArtists = new ArrayList<>();

        ArrayList<Composer> listAllComposer = composerService.getAllComposer();
        for (Composer composer : listAllComposer){
            if (composer instanceof Artist){
                lisArtists.add((Artist) composer);
            }
        }
        return lisArtists;
    }
}
