package com.devcamp.music_api.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.music_api.model.Album;
import com.devcamp.music_api.service.AlbumService;

@RestController
@CrossOrigin
public class AlbumController {
    @Autowired
    AlbumService albumService;

    @GetMapping("/albums")
    public ArrayList<Album> getListAlbums(){
        ArrayList<Album> lAlbums = albumService.getAllAlbums();

        return lAlbums;
    }

}
